package com.company;

public class Main {

    public static void main(String[] args) {
        int x1 = 4;
        int x2 = 2;
        int area1;
        int area2;
        int perimeter1;
        int perimeter2;
        boolean comparison1;
        boolean comparison2;

        area1 = x1 * x1;
        perimeter1 = 4 * x1;
        comparison1 = area1 == perimeter1;
        String result1 = "Периметр дорівнює площі:";
        System.out.println(result1 + " " + comparison1);

        area2 = x2 * x2;
        perimeter2 = 4 * x2;
        comparison2 = area2 == perimeter2;
        String result2 = "Периметр дорівнює площі:";
        System.out.println(result2 + " " + comparison2);

        byte a = 115;
        short b = a;
        int c = b;
        long d = c;
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);

        int e = 234;
        double f = e;
        System.out.println(e);
        System.out.println(f);

        short g = 24_573;
        float h = g;
        double i = h;
        System.out.println(g);
        System.out.println(h);
        System.out.println(i);

        char j = '\u00df';
        int k = j;
        System.out.println(j);
        System.out.println(k);

        int l = 2_147_483_647;
        float m = l;
        System.out.println(l);
        System.out.println(m);

        byte n = 127;
        n += 1;  // n++ postfix
        System.out.println(n);
        ++n;   // prefix
        System.out.println(n);
    }
}
